import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { isMobile } from "react-device-detect";
import styled from 'styled-components';
import AppHead from '../organisms/app-head/app-head';
import AppMenu from '../organisms/app-menu/app-menu';

const PageContainer = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
  grid-template-areas: "AppMenu AppMain";
  color: ${({theme}) => theme.base.textColor};
  background: ${({theme}) => theme.base.background};
  min-height: 100%;
`;

const AppMain = styled.main`
  position: relative;
  grid-area: AppMain;
  background: ${({theme}) => theme.base.background};
  overflow-x: hidden;
`;

class PageTemplate extends PureComponent {
  static propTypes = {
    children: PropTypes.any,
    title: PropTypes.string,
    headerSticky: PropTypes.bool
  };
  constructor(props) {
    super(props);
    this.state = {
      isMenuOpen: false
    };
  }

  componentDidMount() {
    isMobile && document.body.addEventListener('click', this.closeMenu);
  }

  componentWillUnmount() {
    isMobile && document.body.removeEventListener('click', this.closeMenu);
  }

  toggleMenu = () => {
    this.setState({ isMenuOpen: !this.state.isMenuOpen });
  };

  closeMenu = () => {
    this.state.isMenuOpen && this.setState({ isMenuOpen: false });
  };

  render() {
    const { children, headerSticky } = this.props;
    const { isMenuOpen } = this.state;
    return (
      <PageContainer>
        <AppMenu isOpen={isMenuOpen} />
        <AppMain>
          <AppHead isOpen={isMenuOpen} toggleMenu={this.toggleMenu} headerSticky={headerSticky} />
          { children }
        </AppMain>
      </PageContainer>
    )
  }
}

export default PageTemplate;
