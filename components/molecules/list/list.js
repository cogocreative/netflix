import styled from 'styled-components';

const List = styled.ul`
  display: block;
  margin: 0;
  padding: 0;
`;

const ListItem = styled.li`
  display: flex;
  align-items: center;
  padding: 1rem;
  &:hover {
     background: ${({ theme }) => theme.color.primary};
  }
`;

export { List, ListItem }
