import React, { PureComponent } from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import config from '../../../config';
import Image from '../../../components/atoms/image/image';

const MovieItemStyle = styled.div`
  position: relative;
  display: inline-block;
  margin: .5rem;
  width: ${({ width }) => width? width : 'auto'};
  a {
    display: block;
    width: 100%;
  }
  &:hover {
    transform: scale(1.1);
  }
`;

class MovieItemBox extends PureComponent {
  render() {
    const { id, title, image, width } = this.props;
    return (
      <MovieItemStyle id={id} image={image} title={title} width={width}>
        <Link href={`movies/movie?id=${id}`} passHref>
          <a><Image src={`${config.apiImagePath}/w${config.apiSmallImageWidth}/${image}`} /></a>
        </Link>
      </MovieItemStyle>
    )
  }
}

export default MovieItemBox;
