import React from 'react';
import styled from 'styled-components';
import { darken, rgba } from 'polished';

import MenuButton from '../../atoms/menu-button/menu-button';
import { FaFilm, FaHome } from 'react-icons/fa';

const AppMenuList = styled.aside`
  position: fixed;
  top: 0; left: 0; bottom: 0;
  z-index: 100;
  color: ${({theme}) => theme.base.textColor};
  background: ${({theme}) => rgba(darken('0.2', theme.base.background), 1)};
  width: ${({isOpen, theme}) => isOpen ? theme.appMenu.openWidth : 0 };
  overflow: hidden;
  @media(min-width: ${({theme}) => theme.breakpoints.sm}) {
    position: relative;
    grid-area: AppMenu;
    width: ${({theme, isOpen}) => isOpen ? theme.appMenu.openWidth : theme.appMenu.width };
  }
`;

const AppMenu = ({ isOpen }) => {
    return (
        <AppMenuList isOpen={isOpen}>
            <MenuButton title="Netflix home" subTitle="See What's new" icon={<FaHome />} link="/" />
            <MenuButton title="Movies" subTitle="The latest movies" icon={<FaFilm />} link="/movies" active />
        </AppMenuList>
    )
};


export default AppMenu;
