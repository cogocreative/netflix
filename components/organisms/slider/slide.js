import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const SlideStyle = styled.div`
  position: relative;
  display: inline-block;
  height: 100%;
  width: 100%;
  background: url(${({ image }) => image}) no-repeat top right;
  background-size: cover;
  &::after {
    content: "";
    position: absolute;
    top: 0; left: 0; right: 0;
    background: linear-gradient(to bottom, rgba(0,0,0, 0.8) 0%, 
                rgba(0,0,0, 0.5) 15%, rgba(0,0,0, 0.3) 20%, 
                transparent 50%, rgba(0,0,0, 0.2) 60%, 
                ${({theme}) => theme.base.background} 100%);
    height: 100%;
  }
`;

const Slide =  React.forwardRef((props, ref) => <SlideStyle image={props.image} ref={ref}>{props.children}</SlideStyle>);

Slide.propTypes = {
  image: PropTypes.string,
  children: PropTypes.any
};

export default Slide;
