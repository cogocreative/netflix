import Link from 'next/link';
import React, { PureComponent } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Slide from '../../organisms/slider/slide';
import Image from '../../../components/atoms/image/image';
import Button from '../../../components/atoms/button/button';
import { FaArrowAltCircleRight } from 'react-icons/fa';

const SliderWrapper = styled.section`
  position: relative;
  width: 100%;
  margin: 0 auto;
  height: ${({height}) => height};
  overflow: hidden;
  white-space: nowrap;
  transform: translateX(${({ state }) => state.translateValue}px);
  transition: transform ease-out 0.45s;
`;

const SliderInner = styled.div`
   position: relative;
   height: 100%;
   width: 100%;
`;

const SlideContent = styled.div`
  white-space: normal;
  position: absolute;
  top: 10%;
  @media(min-width: ${({ theme }) => theme.breakpoints.sm}) {
    max-width: 60%;
  }
  @media(min-width: ${({ theme }) => theme.breakpoints.md}) {
    max-width: 40%;
  }
  z-index: 1;
  padding: 2rem;
`;

class Slider extends PureComponent {
  static propTypes = {
    list: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      title: PropTypes.string.isRequired,
      titleImage: PropTypes.string.isRequired,
      subTitle: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired,
      link: PropTypes.string.isRequired
    })).isRequired,
    height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    speed: PropTypes.number,
    autoPlay: PropTypes.bool
  };

  static defaultProps = {
    height: '60vh',
    speed: 5000
  };

  state = {
    currentIndex: 0,
    translateValue: 0,
  };
  slideRef = React.createRef();

  componentDidMount() {
   this.props.autoPlay && setInterval(this.nextSlide, this.props.speed);
  }

  prevSlide = () => {
    const { index, translateValue, setTranslateValue, setIndex } = this.props;
    if(Number(index) === 0) return;

    setTranslateValue(translateValue + this.slideRef.current.clientWidth);
    setIndex(index - 1)
  };

  nextSlide = () => {
    if(this.state.currentIndex === this.props.list.length - 1) {
      return this.setState({
        currentIndex: 0,
        translateValue: 0
      })
    }
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex + 1,
      translateValue: prevState.translateValue + -(this.slideRef.current.clientWidth)
    }));
  };

  render() {
    const { height, list } = this.props;
    return (
      <SliderWrapper state={this.state} height={height}>
        <SliderInner>
          {list && list.map((item) => {
            return (
              <Slide key={item.id} image={item.image} ref={this.slideRef}>
                <SlideContent>
                  <Image src={item.titleImage} />
                  <p>{item.description}</p>
                  <Link href={'movies/movie?id=346910'} passHref>
                    <a><Button><FaArrowAltCircleRight /> Play</Button></a>
                  </Link>
                </SlideContent>
              </Slide>
            )
          })}
        </SliderInner>
      </SliderWrapper>
    )
  }
}

export default Slider;
