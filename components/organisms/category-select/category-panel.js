import React, { PureComponent } from 'react';
import { darken, rgba } from 'polished';
import styled from 'styled-components';

const CategoryPanelStyle = styled.div`
  position: absolute;
  height: 320px;
  left: 0;
  margin-top: 10px;
  width: 100%;
  overflow-y: auto;
  background: ${({ theme }) => rgba(darken('0', theme.base.background), 0.98)};
  z-index: 10;
  display: ${({ isOpen }) => isOpen ? 'block' : 'none' }
`;

class CategoryPanel extends PureComponent {
  render() {
    return (
        <CategoryPanelStyle isOpen={this.props.isOpen}>
          {this.props.children}
        </CategoryPanelStyle>
    )
  }
}

export default CategoryPanel;
