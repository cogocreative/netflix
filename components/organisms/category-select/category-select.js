import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import styled from 'styled-components';
import { getMovieGenres } from '../../../store/actions/movies/genre/genre-actions';
import { List, ListItem } from '../../../components/molecules/list/list';
import { FaChevronCircleDown } from 'react-icons/fa';
import CategoryPanel from './category-panel';

const CategorySelectWrapper = styled.div`
  cursor: pointer;
  padding: 0.6rem 1rem;
`;

const Icon = styled.div`
    svg {
      transform: ${({ isOpen }) => isOpen? 'rotate(180deg)' : 'rotate(0deg)' }
    }
`;

class CategorySelect extends PureComponent {
  static propTypes = {
    isLoading: PropTypes.bool,
    results: PropTypes.array,
    getMovieGenres: PropTypes.func,
    onSelect: PropTypes.func.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    }
  }

  componentDidMount() {
    this.props.getMovieGenres();
  }

  toggleCategories = () => {
    this.setState({ isOpen: !this.state.isOpen })
  };

  render() {
    const { results } = this.props;
    const { isOpen } = this.state;
    return (
      <CategorySelectWrapper onClick={this.toggleCategories}>
        <Icon isOpen={isOpen}>
          <FaChevronCircleDown />
        </Icon>
        <CategoryPanel isOpen={isOpen}>
          <List>
            { results && results.map((item) => <ListItem key={item.id} onClick={() => this.props.onSelect(item.name)}>{item.name}</ListItem>) }
          </List>
        </CategoryPanel>
      </CategorySelectWrapper>
    )
  }
}

export default connect(
  (state) => ({
    isLoading: state.movies.genres.isLoading,
    results: state.movies.genres.results,
  }),
  {
    getMovieGenres,
  }
)(CategorySelect);
