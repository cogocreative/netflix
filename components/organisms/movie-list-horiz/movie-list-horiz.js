import React, { PureComponent } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import MovieItemBox from '../../molecules/movie-item-box/movie-item-box';

const MoviesListWrapper = styled.section`
  display: block;
  max-width: 100%;
  padding: 0 1.5rem 0 1.5rem;
  overflow-x: auto;
  overflow-y: hidden;
  white-space: nowrap;
`;

const MovieListInner = styled.div`
  margin-left: -0.5rem;
  margin-right: -0.5rem;
`;

const MovieListTitle = styled.h4`
  margin: .6rem 0;
`;

class MovieListHoriz extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    list: PropTypes.array,
    isLoading: PropTypes.bool
  };
  render() {
    const { title, list, isLoading } = this.props;
    return (
    <MoviesListWrapper>
      <MovieListTitle>{title}</MovieListTitle>
      <MovieListInner>
        { list && list.map((item) => {
          return <MovieItemBox key={item.id} title={item.title} image={item.poster_path} />
        })}
      </MovieListInner>
    </MoviesListWrapper>
    )
  }
}

export default MovieListHoriz;
