import React, { Fragment, PureComponent } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import MovieItemBox from '../../molecules/movie-item-box/movie-item-box';

const MoviesGridWrapper = styled.section`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  padding: ${({ withTitle }) => withTitle ? '0 1.5rem 1.5rem 1.5rem' : '1.5rem'};
  @media(min-width: ${({ theme }) => theme.breakpoints.sm}) {
    grid-template-columns: repeat(4, 1fr);
  }
  @media(min-width: ${({ theme }) => theme.breakpoints.md}) {
    grid-template-columns: repeat(5, 1fr);
  }
  max-width: 100%;
`;

const Title = styled.h4`
  font-size: 1.1rem;
  margin: .3rem 0;
  font-weight: 600;
  padding: 0 1.75rem;
`;

class MovieGrid extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    list: PropTypes.array,
    isLoading: PropTypes.bool
  };
  render() {
    const { title, list, isLoading } = this.props;
    if (isLoading) { return 'Loading...' }
    if (list && list.length <= 0) { return 'Nothing found' }
    return (
      <Fragment>
        { title && <Title>{title}</Title> }
      <MoviesGridWrapper withTitle={title}>
        { list && list.map((item) => {
          return <MovieItemBox key={item.id} id={item.id} title={item.title} image={item.poster_path} />
        })}
      </MoviesGridWrapper>
      </Fragment>
    )
  }
}

export default MovieGrid;
