import React from 'react';
import styled from 'styled-components';
import netflixLogo from '../../../assets/images/netflix-logo.svg';
import Avatar from '../../atoms/avatar/avatar';
import HamburgerButton from '../../atoms/hamburger-button/hamburger-button';

const AppHeadContainer = styled.header`
  position: ${({ headerSticky }) => headerSticky? 'sticky': 'fixed'};
  left: 0;
  top: 0; right: 0;
  @media(min-width: ${({theme}) => theme.breakpoints.sm}) {
    left: ${({isOpen, theme}) => isOpen ? theme.appMenu.openWidth : theme.appMenu.width};
  }
  z-index: 10;
  height: ${({theme}) => theme.header.height};
  background: ${({theme}) => theme.header.background};
`;

const AppHeadInner = styled.div`
  display: grid;
  grid-template-columns: auto 1fr auto;
  align-items: center;
  padding: 0 1rem 0 0;
  height: ${({theme}) => theme.header.height};
`;

const AppHead = ({ isOpen, toggleMenu, headerSticky }) => (
  <AppHeadContainer isOpen={isOpen} headerSticky={headerSticky}>
    <AppHeadInner>
      <HamburgerButton isOpen={isOpen} onClick={toggleMenu} />
      <img src={netflixLogo} height={20} />
      <Avatar image="/static/images/profile.jpg" size={30} />
    </AppHeadInner>
  </AppHeadContainer>
);

export default AppHead;
