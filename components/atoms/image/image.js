import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Img = styled.img`
  display: block;
`;

const Image = ({ src, width, height, style, className }) => <Img src={src} width={width} height={height} style={style} className={className} />;

Image.propTypes = {
  src: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  style: PropTypes.object,
  className: PropTypes.string,
};

Image.defaultProps = {
  width: '100%',
  height: 'auto'
};

export default Image;
