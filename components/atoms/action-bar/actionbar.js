import styled from 'styled-components';

const ActionBar = styled.div`
  display: flex;
  align-items: center;
`;

export default ActionBar;
