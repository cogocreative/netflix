import React from 'react';
import styled from 'styled-components';
import { FaBars } from 'react-icons/fa';

const ButtonContainer = styled.div`
    padding: 1.2rem 1rem 1rem 1rem;
    cursor: pointer;
    color: ${({ theme, isOpen }) => isOpen ? theme.color.primary : theme.base.textColor };
    transition: color 0.1s ease;
`;

const HamburgerButton = ({ isOpen, onClick }) => (
  <ButtonContainer isOpen={isOpen} onClick={onClick}><FaBars /></ButtonContainer>
);

export default HamburgerButton;
