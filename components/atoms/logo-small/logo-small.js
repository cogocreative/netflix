import React from 'react';
// I don't like these paths. Find out from next.js docs how to solve this.
import netflixLogo from '../../../assets/images/netflix-logo-small.svg';

const LogoSmall = ({ width }) => <img src={netflixLogo} width={width || '20px'} title="Netflix" />;

export default LogoSmall;
