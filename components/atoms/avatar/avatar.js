import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Image from '../image/image';

const AvatarStyle = styled(Image)`
  border-radius: 50%;
`;

const Avatar = ({ image, size }) => <AvatarStyle src={image} width={size} height={size} />;

Avatar.propTypes = {
  image: PropTypes.string.isRequired,
  size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

export default Avatar;
