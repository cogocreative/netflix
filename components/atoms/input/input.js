import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const InputStyle = styled.input`
  padding: 0 1rem;
  height: 40px;
  color: ${({ theme }) => theme.input.textColor};
  background: ${({ theme }) => theme.input.background};
  border: ${({ theme }) => theme.input.border};
  outline: ${({ theme }) => theme.input.border};
  &:hover {
    color: ${({ theme }) => theme.input.hover.textColor};
    background: ${({ theme }) => theme.input.hover.background};
    border: ${({ theme }) => theme.input.hover.border};
    outline: ${({ theme }) => theme.input.hover.border};
  }
  &:focus {
    color: ${({ theme }) => theme.input.focus.textColor};
    background: ${({ theme }) => theme.input.focus.background};
    border: ${({ theme }) => theme.input.focus.border};
    outline: ${({ theme }) => theme.input.focus.border};
  }
  width: 100%;
`;

const Input = (props) => <InputStyle {...props} name={props.name} type={props.type} value={props.value} placeholder={props.placeholder} onClick={props.onClick} onChange={(props.onChange)} />;

Input.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.any,
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  onClick: PropTypes.func,
  onChange: PropTypes.func
};

export default Input;
