import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ButtonStyle = styled.button`
  display: inline-block;
  vertical-align: middle;
  outline: none;
  border: none;
  border-radius: 5px;
  padding: .8rem 1.5rem;
  color: ${({theme}) => theme.base.textColor};
  background: ${({theme, color}) => color ? theme.color[color] : theme.color.primary};
  cursor: pointer;
  &:hover {
    box-shadow: 0 2px 4px rgba(0,0,0,0.75);
  }
`;

const Button = ({ children }) => <ButtonStyle>{children}</ButtonStyle>;

Button.propTypes = {
  children: PropTypes.any,
  color: PropTypes.string
};

export default Button;
