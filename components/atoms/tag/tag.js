import styled from 'styled-components';

const Tag = styled.div`
  display: inline-block;
  border-radius: 15px;
  font-size: 0.9rem;
  font-weight: 500;
  background: ${({theme}) => theme.color.secondary};
  margin: 0.2rem;
  padding: 0.3rem 1rem;
`;

export default Tag;
