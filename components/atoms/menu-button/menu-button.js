import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Link from 'next/link';
import { withRouter } from 'next/router';

const MenuButtonStyle = styled.a`
  display: flex;
  align-items: center;
  flex-wrap: nowrap;
  padding: 1.2rem 0 1rem 0;
  overflow: hidden;
  color: ${({theme}) => theme.appMenu.iconColor};
  transition: color 0.3s ease;
  text-decoration: none;
  width: ${({theme}) => theme.appMenu.openWidth};
  &:hover {
    color: ${({theme}) => theme.appMenu.hover.iconColor};
    background: ${({theme}) => theme.appMenu.hover.background};
  }
  ${({router, href, theme}) => router && router.pathname === href ? `color: ${theme.appMenu.active.iconColor};background: ${theme.appMenu.active.background}` : ''};
`;

const IconContainer = styled.div`
  display: inline-block;
  vertical-align: middle;
  width: ${({theme}) => theme.appMenu.width};
  text-align: center;
`;

const TextContainer = styled.div`
  display: inline-block;
`;

const Title = styled.div`
  color: ${({theme}) => theme.appMenu.textColor};
  text-decoration: none;
`;

const SubTitle = styled.div`
  font-size: .7rem;
  opacity: 0.6;
`;

const MenuButton = ({ title, subTitle, icon, link, router, href }) => (
  <Link href={link} prefetch passHref>
    <MenuButtonStyle router={router} href={href} title={title}>
      <IconContainer>
        {icon}
      </IconContainer>
      <TextContainer>
        <Title>{title}</Title>
        <SubTitle>{subTitle}</SubTitle>
      </TextContainer>
    </MenuButtonStyle>
  </Link>
);

MenuButton.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.any.isRequired,
  link: PropTypes.string.isRequired,
  active: PropTypes.bool
};

export default withRouter(MenuButton);
