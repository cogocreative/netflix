import styled from 'styled-components';

const Title = styled.h1`
  font-size: 1.4rem;
  font-weight: 600;
  background: rgba(0,0,0,0.3);
  margin: 0;
  padding: 1.5rem;
`;

export default Title;
