import React, { PureComponent } from 'react';
import {connect} from 'react-redux';
import PageTemplate from '../components/templates/page-template';
import Slider from '../components/organisms/slider/slider';
import MovieGrid from '../components/organisms/movie-grid/movie-grid';
import { getFeaturedMovies } from '../store/actions/movies/featured/featured-actions';
import { getLatestMovies } from '../store/actions/movies/latest/latest-actions';

class Index extends PureComponent {
  static async getInitialProps ({ reduxStore }) {
    const featured = await reduxStore.dispatch(getFeaturedMovies());
    const latest = await reduxStore.dispatch(getLatestMovies());
    return { featured, latest };
  }
  render() {
    const {
      featuredResults,
      latestLoading,
      latestResults
    } = this.props;
    return (
      <PageTemplate title="Home">
        <Slider list={featuredResults} />
        <MovieGrid title="Latest movies" list={latestResults} isLoading={latestLoading} />
      </PageTemplate>
    )
  }
}

export default connect(
  (state) => ({
    latestLoading: state.movies.latest.isLoading,
    latestResults: state.movies.latest.results,
    featuredResults: state.movies.featured.data,
  }),
  {
    getFeaturedMovies,
    getLatestMovies
  }
)(Index);
