import App, {Container} from 'next/app';
import React from 'react';
import { Provider } from 'react-redux';
import { createGlobalStyle } from 'styled-components';
import { ThemeProvider } from 'styled-components';
import withReduxStore from '../utils/withReduxStore';
import theme from '../theme/default.theme';

const GlobalStyle = createGlobalStyle`
  html, body, #__next {
    margin: 0;
    padding: 0;
    font-size: 16px;
    height: 100%;
    font-family: 'Lato', sans-serif;
    -webkit-font-smoothing: antialiased;
  }
  *, *:before, *:after {
    box-sizing: border-box;
    transition: all .4s ease;
  }
`;


class MyApp extends App {
  render () {
    const {Component, pageProps, reduxStore} = this.props;
    return (
      <Container>
        <Provider store={reduxStore}>
          <ThemeProvider theme={theme}>
            <Component {...pageProps} />
          </ThemeProvider>
        </Provider>
        <GlobalStyle />
      </Container>
    )
  }
}

export default withReduxStore(MyApp)
