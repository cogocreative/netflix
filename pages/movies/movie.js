import React, { Fragment, PureComponent } from 'react';
import { withRouter } from 'next/router';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { WaveLoading } from 'styled-spinkit';
import Tag from '../../components/atoms/tag/tag';
import PageTemplate from '../../components/templates/page-template';
import Title from '../../components/atoms/title/title';
import MovieItemBox from '../../components/molecules/movie-item-box/movie-item-box';
import { getMovie } from '../../store/actions/movies/detail/detail-actions';

const MovieContent = styled.div`
  display: grid;
  padding: 1.5rem;
  @media(min-width: ${({ theme }) => theme.breakpoints.sm}) {
    grid-template-columns: auto 1fr;
  }
`;

const MovieImage = styled.div`
  width: 100%;
`;

const MovieOverview = styled.div`
  padding: 0 0.7rem ;
  @media(min-width: ${({ theme }) => theme.breakpoints.sm}) {
    padding: 0 1.5rem ;
  }
`;

const Genres = styled.div`
  display: block;
  padding: 1rem 0;
`;

class MovieDetail extends PureComponent {

  componentDidMount() {
    this.loadMovie();
  }

  loadMovie = () => {
    this.props.getMovie(this.props.router.query.id)
  };

  render() {
    const { isLoading, data } = this.props;
    return (
      <PageTemplate title='Movie detail' headerSticky>
        {isLoading ? <WaveLoading /> : <Fragment>
          <Title>{data.title}</Title>
          <MovieContent>
            <MovieImage>
              <MovieItemBox id={data.id} title={data.title} image={data.poster_path} width="100%" />
            </MovieImage>
            <MovieOverview>
              {data.overview}
              <Genres>
                {data.genres && data.genres.map((genre) => {
                  return <Tag key={genre.id}>{genre.name}</Tag>
                })}
              </Genres>
            </MovieOverview>
          </MovieContent>
        </Fragment>}
      </PageTemplate>
    )
  }
}

export default connect(
  (state) => ({
    isLoading: state.movies.detail.isLoading,
    data: state.movies.detail.data,
  }),
  {
    getMovie
  }
)(withRouter(MovieDetail));
