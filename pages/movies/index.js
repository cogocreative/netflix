import React, { PureComponent } from 'react';
import {connect} from 'react-redux';
import debounce from '../../utils/debounce';
import { searchMovies } from '../../store/actions/movies/search/search-actions';
import PageTemplate from '../../components/templates/page-template';
import ActionBar from '../../components/atoms/action-bar/actionbar';
import MovieGrid from '../../components/organisms/movie-grid/movie-grid';
import Input from '../../components/atoms/input/input';
import CategorySelect from '../../components/organisms/category-select/category-select';

class MoviesPage extends PureComponent {
  static async getInitialProps ({ reduxStore }) {
    const search = await reduxStore.dispatch(searchMovies());
    return { search };
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  onChange = (event) => {
    const { name, value } = event.target;
    this.setState({ ...this.state, [name]: value }, this.loadData);
  };

  setCategory = (category) => {
    this.setState({ ...this.state, category }, this.loadData)
  };

  loadData = debounce(() => {
      this.props.searchMovies(this.state);
  }, 1000);

  render(){
    const { isLoading, results } = this.props;
    return <PageTemplate title="Home" headerSticky>
      <ActionBar>
        <Input name="search" type="text" placeholder="Search for a movie..." onChange={this.onChange} />
        <CategorySelect onSelect={this.setCategory} />
      </ActionBar>
      <MovieGrid isLoading={isLoading} list={results} />
    </PageTemplate>
  }
}

export default connect(
  (state) => ({
    isLoading: state.movies.search.isLoading,
    results: state.movies.search.results,
  }),
  {
    searchMovies
  }
)(MoviesPage);

