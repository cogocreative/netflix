import { combineReducers } from 'redux';
import topRated from './top-rated/top-rated-reducer';
import featured from './featured/featured-reducer';
import latest from './latest/latest-reducer';
import search from './search/search-reducer';
import genres from './genre/genre-reducer';
import detail from './detail/detail-reducer';

const reducers = combineReducers({
  search,
  topRated,
  featured,
  latest,
  genres,
  detail
});

export default reducers;
