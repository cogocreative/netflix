import { GET_MOVIE_DETAIL_STARTED, GET_MOVIE_DETAIL, GET_MOVIE_DETAIL_ERROR } from '../../../actions/movies/detail/detail-actions';

const initialState = {
  isLoading: false,
  data: {}
};

const reducer = (state = initialState, action) => {
  const { type, payload = {} } = action;
  switch ( type ) {
    case GET_MOVIE_DETAIL_STARTED:
      return { ...state, isLoading: true };

    case GET_MOVIE_DETAIL: {
      return {
        ...state,
        isLoading: false,
        data: payload
      };
    }

    case GET_MOVIE_DETAIL_ERROR: {
      return {
        ...state,
        isLoading: false,
        data: payload
      };
    }
    default:
      return state;
  }
};

export default reducer;
