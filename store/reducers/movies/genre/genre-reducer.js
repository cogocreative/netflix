import { GET_MOVIES_GENRE_STARTED, GET_MOVIES_GENRE, GET_MOVIES_GENRE_ERROR } from '../../../actions/movies/genre/genre-actions';

const initialState = {
  isLoading: false,
  results: []
};

const reducer = (state = initialState, action) => {
  const { type, payload = {} } = action;
  switch ( type ) {
    case GET_MOVIES_GENRE_STARTED:
      return { ...state, isLoading: true };

    case GET_MOVIES_GENRE: {
      return {
        ...state,
        isLoading: false,
        results: payload.genres
      };
    }

    case GET_MOVIES_GENRE_ERROR: {
      return {
        ...state,
        isLoading: false,
        data: payload
      };
    }
    default:
      return state;
  }
};

export default reducer;
