import { GET_MOVIES_LATEST_STARTED, GET_MOVIES_LATEST, GET_MOVIES_LATEST_ERROR } from '../../../actions/movies/latest/latest-actions';

const initialState = {
  isLoading: false,
  page: 1,
  total_results: 0,
  total_pages: 0,
  results: []
};

const reducer = (state = initialState, action) => {
  const { type, payload = {} } = action;
  switch ( type ) {
    case GET_MOVIES_LATEST_STARTED:
      return { ...state, isLoading: true };

    case GET_MOVIES_LATEST: {
      return {
        ...state,
        isLoading: false,
        page: payload.page,
        total_results: payload.total_results,
        total_pages: payload.total_pages,
        results: payload.results
      };
    }

    case GET_MOVIES_LATEST_ERROR: {
      return {
        ...state,
        isLoading: false,
        data: payload
      };
    }
    default:
      return state;
  }
};

export default reducer;
