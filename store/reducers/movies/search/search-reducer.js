import { SEARCH_MOVIES_STARTED, SEARCH_MOVIES, SEARCH_MOVIES_ERROR } from '../../../actions/movies/search/search-actions';

const initialState = {
  isLoading: false,
  page: 1,
  total_results: 0,
  total_pages: 0,
  results: []
};

const reducer = (state = initialState, action) => {
  const { type, payload = {} } = action;
  switch ( type ) {
    case SEARCH_MOVIES_STARTED:
      return { ...state, isLoading: true };

    case SEARCH_MOVIES: {
      return {
        ...state,
        isLoading: false,
        page: payload.page,
        total_results: payload.total_results,
        total_pages: payload.total_pages,
        results: payload.results
      };
    }

    case SEARCH_MOVIES_ERROR: {
      return {
        ...state,
        isLoading: false,
        data: payload
      };
    }
    default:
      return state;
  }
};

export default reducer;
