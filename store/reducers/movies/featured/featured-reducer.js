  import { GET_MOVIES_FEATURED_STARTED, GET_MOVIES_FEATURED, GET_MOVIES_FEATURED_ERROR } from '../../../actions/movies/featured/featured-actions';

const initialState = {
  data:[]
};

const reducer = (state = initialState, action) => {
  const { type, payload = {} } = action;
  switch ( type ) {
    case GET_MOVIES_FEATURED_STARTED:
      return { ...state, isLoading: true };

    case GET_MOVIES_FEATURED: {
      return {
        ...state,
        isLoading: false,
        data: payload
      };
    }

    case GET_MOVIES_FEATURED_ERROR: {
      return {
        ...state,
        isLoading: false,
        data: payload
      };
    }
    default:
      return state;
  }
};

export default reducer;
