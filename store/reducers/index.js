import { combineReducers } from 'redux';
import movies from './movies/index';

const reducers = combineReducers({
  movies
});

export default reducers;
