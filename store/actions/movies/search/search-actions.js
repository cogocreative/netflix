import Fetch from '../../../../utils/fetch'
export const SEARCH_MOVIES_STARTED = '@@netflix/movies/SEARCH_MOVIES_STARTED';
export const SEARCH_MOVIES = '@@netflix/movies/SEARCH_MOVIES';
export const SEARCH_MOVIES_ERROR = '@@netflix/movies/SEARCH_MOVIES_ERROR';

export const searchMovies = (params = {}) => async (dispatch) => {
  const { search, category } = params;
  try {
    dispatch({ type: SEARCH_MOVIES_STARTED });
    const query = (search || category ) ? await Fetch(`search/movie?query=${search}&category=${category}`) : await Fetch(`movie/upcoming`);
    const data = await query.data;
    dispatch({ type: SEARCH_MOVIES, payload: data });
  } catch (error) {
    dispatch({ type: SEARCH_MOVIES_ERROR, payload: error });
  }
};
