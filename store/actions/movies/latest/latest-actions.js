import Fetch from '../../../../utils/fetch'
export const GET_MOVIES_LATEST_STARTED = '@@netflix/movies/GET_MOVIES_LATEST_STARTED';
export const GET_MOVIES_LATEST = '@@netflix/movies/GET_MOVIES_LATEST';
export const GET_MOVIES_LATEST_ERROR = '@@netflix/movies/GET_MOVIES_LATEST_ERROR';

export const getLatestMovies = (params) => async (dispatch) => {
  try {
    dispatch({ type: GET_MOVIES_LATEST_STARTED });
    const query = await Fetch('movie/upcoming');
    const data = await query.data;
    dispatch({ type: GET_MOVIES_LATEST, payload: data });
  } catch (error) {
    dispatch({ type: GET_MOVIES_LATEST_ERROR, payload: error });
  }
};
