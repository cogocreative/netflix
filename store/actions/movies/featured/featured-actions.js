export const GET_MOVIES_FEATURED_STARTED = '@@netflix/movies/GET_MOVIES_FEATURED_STARTED';
export const GET_MOVIES_FEATURED = '@@netflix/movies/GET_MOVIES_FEATURED';
export const GET_MOVIES_FEATURED_ERROR = '@@netflix/movies/GET_MOVIES_FEATURED_ERROR';

const data =  [
  {
    id: '1',
    title: 'test',
    titleImage: '/static/images/maniac-title.png',
    subTitle: 'Watch the limited series now',
    description: 'An experimental drug promises to rewire their troubled minds -- no side effect, no complications. But things don\'t go as planned',
    image: '/static/images/maniac.jpg',
    link: '/some-movie'
  },
  {
    id: '2',
    title: 'test 2',
    titleImage: '/static/images/maniac-title.png',
    subTitle: 'Watch the limited series tomorrow',
    description: 'An experimental drug promises to rewire their troubled minds -- no side effect, no complications. But things don\'t go as planned',
    image: '/static/images/car-masters.jpg',
    link: '/some-movie'
  }
];

  export const getFeaturedMovies = () => async (dispatch) => {
  try {
    dispatch({ type: GET_MOVIES_FEATURED_STARTED });
    dispatch({ type: GET_MOVIES_FEATURED, payload: data });
  } catch (error) {
    dispatch({ type: GET_MOVIES_FEATURED_ERROR, payload: error });
  }
};
