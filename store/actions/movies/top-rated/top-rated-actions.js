import Fetch from '../../../../utils/fetch'
export const GET_MOVIES_TOP_RATED_STARTED = '@@netflix/movies/GET_MOVIES_TOP_RATED_STARTED';
export const GET_MOVIES_TOP_RATED = '@@netflix/movies/GET_MOVIES_TOP_RATED';
export const GET_MOVIES_TOP_RATED_ERROR = '@@netflix/movies/GET_MOVIES_TOP_RATED_ERROR';

export const getTopRatedMovies = (params) => async (dispatch) => {
  try {
    dispatch({ type: GET_MOVIES_TOP_RATED_STARTED });
    const query = await Fetch('movie/top_rated');
    const data = await query.data;
    dispatch({ type: GET_MOVIES_TOP_RATED, payload: data });
  } catch (error) {
    dispatch({ type: GET_MOVIES_TOP_RATED_ERROR, payload: error });
  }
};
