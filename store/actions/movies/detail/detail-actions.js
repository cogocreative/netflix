import Fetch from '../../../../utils/fetch'
export const GET_MOVIE_DETAIL_STARTED = '@@netflix/movies/GET_MOVIE_DETAIL_STARTED';
export const GET_MOVIE_DETAIL = '@@netflix/movies/GET_MOVIE_DETAIL';
export const GET_MOVIE_DETAIL_ERROR = '@@netflix/movies/GET_MOVIE_DETAIL_ERROR';

export const getMovie = (id) => async (dispatch) => {
  try {
    dispatch({ type: GET_MOVIE_DETAIL_STARTED });
    const query = await Fetch(`movie/${id}`);
    const data = await query.data;
    dispatch({ type: GET_MOVIE_DETAIL, payload: data });
  } catch (error) {
    dispatch({ type: GET_MOVIE_DETAIL_ERROR, payload: error });
  }
};
