import Fetch from '../../../../utils/fetch'
export const GET_MOVIES_GENRE_STARTED = '@@netflix/movies/GET_MOVIES_GENRE_STARTED';
export const GET_MOVIES_GENRE = '@@netflix/movies/GET_MOVIES_GENRE';
export const GET_MOVIES_GENRE_ERROR = '@@netflix/movies/GET_MOVIES_GENRE_ERROR';

export const getMovieGenres = () => async (dispatch) => {
  try {
    dispatch({ type: GET_MOVIES_GENRE_STARTED });
    const query = await Fetch('genre/movie/list');
    const data = await query.data;
    dispatch({ type: GET_MOVIES_GENRE, payload: data });
  } catch (error) {
    dispatch({ type: GET_MOVIES_GENRE_ERROR, payload: error });
  }
};
