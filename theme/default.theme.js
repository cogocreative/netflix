const theme = {};

theme.breakpoints = {
  xs: '240px',
  sm: '640px',
  md: '1300px',
  lg: '1600px',
  xl: '2000px'
};

theme.color = {
  primary: '#D81F26',
  secondary: '#03A9F4'
};

theme.base = {
  background: '#121212',
  textColor: '#efefef',
  hover: {
    textColor: 'white'
  },
  active: {
    textColor: theme.color.primary
  }
};

theme.appMenu = {
  width: '60px',
  openWidth: '280px',
  textColor: '#efefef',
  background: 'transparent',
  iconColor: '#D1D1D1',
  hover: {
    iconColor: theme.base.hover.textColor,
    background: `linear-gradient(to right, ${theme.color.primary} 0px, ${theme.color.primary} 3px, transparent 3px, transparent)`,
  },
  active: {
    iconColor: theme.base.active.textColor,
    background: `linear-gradient(to right, ${theme.color.primary} 0px, ${theme.color.primary} 3px, black 3px, black)`,
  }
};

theme.header = {
  height: '60px',
  background: 'rgba(0,0,0, 0.75)',
};

theme.input = {
  background: 'rgba(0,0,0, 0.6)',
  textColor: theme.base.textColor,
  border: 'none 0',
  hover: {
    background: 'rgba(0,0,0, 0.8)',
    textColor: theme.base.hover.textColor,
    border: 'none 0'
  },
  focus: {
    background: 'rgba(0,0,0, 1)',
    textColor: theme.base.hover.textColor,
    border: 'none 0'
  }
};


export default theme;
