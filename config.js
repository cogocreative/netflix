const config = {
    apiUrl: 'https://api.themoviedb.org/3/',
    apiKey: '7b1559caeff60fd91fede1d1f63dabaf',
    apiImagePath: 'https://image.tmdb.org/t/p',
    apiSmallImageWidth: 185
};

export default config;
