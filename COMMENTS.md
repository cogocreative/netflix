# NOTES

1. Used nextjs for quick set-up.
2. React v16, and styled-components.
3. I've not to use any framework. Instead creating my own components.
4. SSR for fast load times.

## Issues

1. Cors issue with moviedb. *NOW FIXED*
2. Never finished slider
3. Nextjs wouldn't let me get router props on server, so detail page is client side. I need a custom server.js with some malarky for that.
4. moviedb doesn't let me filter by title and genre. Probably should just link the genre tags on the detail page and create a page with actions to pull movies by genre.
