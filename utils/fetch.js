// @flow
import axios from 'axios';
import config from '../config';

// const token = 'some-token-from-auth';

const Fetch = axios.create({
  baseURL: config.apiUrl,
  crossdomain: true,
  headers: {
    'content-type': 'application/x-www-form-urlencoded'
  },
  params: {
    api_key: config.apiKey,
  },
});

export default Fetch;
